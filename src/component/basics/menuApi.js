const Menu = [
    {
      id: 1,
      image: "https://im.rediff.com/getahead/2020/sep/29burnt-garlic-chilli-maggi.jpg",
      name: "maggi",
      category: "breakfast",
      price: "12₹",
      description:
        "I love Maggi realy oo yues  Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
  
    {
      id: 2,
      image: "https://i.ytimg.com/vi/hjsTyL-Ewcs/maxresdefault.jpg",
      name: "allu pakoda",
      category: "evening",
      price: "20₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 3,
      image: "https://natashaskitchen.com/wp-content/uploads/2020/05/Boiled-Corn-on-the-Cob-500x500.jpg",
      name: "corn",
      category: "breakfast",
      price: "10₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 4,
      image: "http://1.bp.blogspot.com/-jhY0v28XUGI/UAnFN1YpYdI/AAAAAAAAJsg/T1fxUFj1MVk/s1600/IMG_4053.JPG",
      name: "chola",
      category: "lunch",
      price: "50₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 5,
      image: "https://jslight.com.np/wp-content/uploads/2022/06/pizza.jpg",
      name: "pizza",
      category: "evening",
      price: "80₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 6,
      image: "https://media-cdn.tripadvisor.com/media/photo-s/0f/5c/41/9c/non-veg-thali.jpg",
      name: "Non-Veg Thali",
      category: "dinner",
      price: "180₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 7,
      image: "https://polkajunction.com/wp-content/uploads/2020/09/Peda-traditional-indian-sweet.jpg",
      name: "Sweets",
      category: "dinner",
      price: "60₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 8,
      image: "https://www.secondrecipe.com/wp-content/uploads/2017/08/rajma-chawal-1.jpg",
      name: "Rajma Rice",
      category: "lunch",
      price: "60₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 9,
      image: "https://resize.indiatvnews.com/en/resize/newbucket/1200_-/2022/05/pjimage-30-1652290478.jpg",
      name: "samaso",
      category: "evening",
      price: "10₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 10,
      image: "https://www.virtualnepali.com/wp-content/uploads/2018/10/wai-wai-chatpate-4.png",
      name: "chatpate",
      category: "evening",
      price: "10₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    {
      id: 11,
      image: "https://i.ytimg.com/vi/lVHk1B8tS0w/maxresdefault.jpg",
      name: "chicken naan",
      category: "evening",
      price: "10₹",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, at consectetur totam voluptatibus quibusdam iusto. Accusamus quas, soluta ipsam autem eius necessitatibus fugiat in . ",
    },
    
  ];
  
  export default Menu;